package main

import "fmt"

func logging() {
	fmt.Println("Slesai memanggil Function")
}

// defer itu digunakan kaya finally, meski dia diatas tetep manggilnyta terakhiran (artinya meski dia error atau enggk tetep dijalankan)
// kalo tanpa defer ya tarok di paling bawah
func runApplication() {
	defer logging()
	fmt.Println("Run Application")
}

func main() {
	runApplication()
}
