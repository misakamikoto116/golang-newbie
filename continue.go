package main

import "fmt"

// Artinya dia cuma ambil yang hasil bagi 2 == 0,
// nantinya bakal nampil yang genap aja
func main() {
	for i := 0; i < 10; i++ {
		if i%2 == 0 {
			continue
		}

		fmt.Println("Perulangan ke", i)
	}
}
