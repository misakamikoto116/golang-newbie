package main

import "fmt"

func main() {
	name := "Joel"
	counter := 0

	// Ini nama var bisa sama tapi dikasih :=, bisa jga pake variabel dnegan nama beda
	// digunakan ini biar name yang diclosure func increment hanya operasi sementara.. sementara diluar closure increment hasilnya tetap name yang diatas / joel
	increment := func() {
		name := "budi"
		fmt.Println("increment")
		counter++
		fmt.Println(name)
	}

	increment()
	increment()

	fmt.Println(counter)
	fmt.Println(name)
}
