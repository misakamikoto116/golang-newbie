package main

import "fmt"

func main() {
	var nilai32 int32 = 129

	// konversi dari int 32 Ke Int 64
	// Konsepnya kaya keulang ulang jika gk cukup kapasiti nya
	var nilai64 int64 = int64(nilai32)
	var nilai8 int8 = int8(nilai32)

	fmt.Println(nilai32)
	fmt.Println(nilai64)
	fmt.Println(nilai8)

	var name = "Joel"
	// Outputnya byte.. jadi J = inputan Byte
	var e byte = name[0]

	// lalu disini Byte dikonversikan ke string
	var eString string = string(e)

	fmt.Println(name)
	fmt.Println(eString)

}
