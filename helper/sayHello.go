package helper

import "fmt"

// gk bisa dipanggil dari luar karena huruf awalnya kecil, harus kapital dlu biar bisa dipanggil dari luar
var version = 1
var Application = "Belajar Golang"

func SayHello(name string) {
	fmt.Println("Hello", name)
}

// ini gak bisa diakses dari luar karena huruf awalnya kecil, jadi kalo mau diakses dengan cara ganti namanya jadi
//(S)ayGoodBye
func sayGoodBye(name string) {
	fmt.Println("Goodbye", name)
}
