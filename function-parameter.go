package main

import "fmt"

func sayHelloTo(firstName string, lastName string) {
	fmt.Println("Hello", firstName, lastName)
}

func main() {
	firstName := "Joel"
	sayHelloTo(firstName, "Joel")
	sayHelloTo("Intan", "effendi")
}
