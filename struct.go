package main

import "fmt"

// untuk penyimpanan data yang paling bagus adalah dengan struct

// name dan address adalah string, age adalah int
// struct ini kaya bkin state cara pakenya langsung customer.name
type Customer struct {
	Name, Address string
	Age           int
}

// Ini Pake Struct Function
func (customer Customer) sayHi(name string) {
	fmt.Println("Hello", name, "My Name is", customer.Name)
}

func (a Customer) sayHuuu() {
	fmt.Println("Huuuuuu from", a.Name)
}

func main() {
	// Cara set structnya
	var orang Customer
	orang.Name = "Joel"
	orang.Address = "Jalan Elang"
	orang.Age = 30

	// Pada intinya orang disini yang akan dieksekusi dan bisa langsung dipake, karena kan dengan kode diatas artinya membuat data baru di struct Customer, nah joko ini parameternya, makanya hasilnya Hello Joko my name is joel
	// orang ini dijadikan variabel pertama / customer Customer
	// Cara menggunakan Struct Function
	orang.sayHi("Joko")

	// Cara kedua set struct
	orangKedua := Customer{
		Name:    "Intan",
		Address: "Indramayu",
		Age:     19,
	}

	// yang ini jga sama.. tetap intance ke orang yang diatas.. krena orang = new Customer
	orangKedua.sayHuuu()

	// Cara Sederhana (ketiga), harus sesuai urutan property struct, yang ini rentan error karena kalo urutannya beda ini jga error
	// budi := Customer{"budi", "Indonesia", 30}
}
