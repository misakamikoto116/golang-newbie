package helper

import "fmt"

// masih bisa pake say hello karena packagenya beda, tapi kalo pake package helper gk bisa karena udah ada
func SayHello(name string) {
	fmt.Println("Hello", name)
}
