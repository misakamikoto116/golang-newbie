package main

import "fmt"

func random() interface{} {
	return "Joel"
}

// syntax result.(type) untuk mengetahui type datanya apa
// Kenapa pake interface kosong? karena isinya bisa bebas, misal enth itu string atau int atau array dll.. makanya di bawah ini perlu dicek lagi apakah isinya apa
func main() {
	var result interface{} = random()
	// convert ke string
	//var resultString string = result.(string)
	//fmt.Println(resultString)

	switch value := result.(type) {
	case string:
		fmt.Println("Value", value, "is string")
	case int:
		fmt.Println("Value", value, "is int")
	default:
		fmt.Println("Unknown type")
	}
}
