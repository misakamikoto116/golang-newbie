package main

import (
	"errors"
	"fmt"
)

// outputnya int dan interface error
// Jika gk error maka errornya dijadiin nil aja
func pembagi(nilai int, pembagi int) (int, error) {
	if pembagi == 0 {
		return 0, errors.New("Pembagi tidak boleh 0")
	} else {
		result := nilai / pembagi
		return result, nil
	}
}

// Buat variabel hasil = result
// variabel kedua / err = errornya
// sama kaya js :
// [hasil, error] = pembagi(100, 0)
func main() {
	hasil, err := pembagi(100, 0)
	if err == nil {
		fmt.Println("hasil", hasil)
	} else {
		fmt.Println("Error", err.Error())
	}
}
