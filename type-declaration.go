// Konsepnya sih bikin tipe data baru / shortcut dengan tipe data yang udah ada
package main

import "fmt"

func main() {
	type NoKtp string
	type married bool

	var noKtpJoel NoKtp = "27172791919203"
	var marriedstatus married = false
	fmt.Println(noKtpJoel)
	fmt.Println(marriedstatus)
}
