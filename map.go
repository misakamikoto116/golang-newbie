package main

import "fmt"

// Map Lebih fleksibel dari array dan slice
// Karena dia bisa ditentukan sendiri
func main() {

	// membuat map baru tapi dikasih value
	person := map[string]string{
		"name":    "Joel",
		"address": "Samarinda",
	}

	person["title"] = "Programmer"

	fmt.Println(person)
	fmt.Println(person["name"])
	fmt.Println(person["address"])

	// Membuat map baru
	var book map[string]string = make(map[string]string)
	book["title"] = "Belajar Go Lang"
	book["author"] = "Julian"
	book["ups"] = "Salah"
	fmt.Println(book)
	fmt.Println(len(book))

	// cara hapus property
	delete(book, "ups")

	fmt.Println(book)
	fmt.Println(len(book))
}
