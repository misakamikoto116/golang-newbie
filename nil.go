// nil itu seperti null, cuman bedanya golang jika datanya gak ada biasanya bakal jdi default value.. misal string jadi "" integer jadi 0, boolean jadi false dll

package main

import "fmt"

// returnnya map, paramnya name
func newMap(name string) map[string]string {
	if name == "" {
		return nil
	} else {
		return map[string]string{
			"name": name,
		}
	}
}

func main() {
	var person map[string]string = newMap("Joel")

	if person == nil {
		fmt.Println("Data Kosong")
	} else {
		fmt.Println(person)
	}
}
