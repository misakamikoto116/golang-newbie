package main

import "fmt"

// Recover digunakan untuk menolak error, artinya mirip kaya catch hanya saja dibikin by json bukan by throw error
// nil => artinya tidak ada panic yang perlu direcover
func endApp() {
	message := recover()
	if message != nil {
		fmt.Println("Error Dengan Message", message)
	}
	fmt.Println("Aplikasi Selesai")
}

// defer tetap dieksekusi setelah aplikasi berjalan, tapi karena tidak sampe sana (duluan kena panic).. maka yang tampil cuma aplikasi selesai, yaitu defer / aplikasi selesai, yang bener aja artinya adalah finally mau error mau enggk tetap dijalanin, tapi bisa aja dijalankan paling akhir tergantung urutan func defernya seperti endApp yang sekarang
func runApp(error bool) {
	defer endApp()
	if error {
		panic("Aplikasi ERROR")
	}

	fmt.Println("Aplikasi Berjalan")
}

func main() {
	runApp(true)
	fmt.Println("Joel")

	// biar gk ada pesan error
	// runApp(false)
}
