package main

import "fmt"

func getGoodBye(name string) string {
	return "Good Bye" + name
}

// gb adalah getGoodbye function, anggap aja bkin shortcut
func main() {
	gb := getGoodBye

	fmt.Println(gb("Joel"))

	result := gb
	fmt.Println(result("Joel"))
}
