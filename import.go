package main

import (
	"fmt"
	"pemula/helper"
)

func main() {
	helper.SayHello("Eko")
	// helper.sayGoodbye("Eko") // error karena hurufnya kecil / hanya private
	fmt.Println(helper.Application)
	// fmt.Println(helper.version) // error
}
