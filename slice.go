package main

import "fmt"

func main() {
	// Dalamnya bisa berubah ubah isinya dan jumlahnya, tapi pada akhirnya bakal diconvert ke array
	var months = [...]string{
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember",
	}

	// array[low:high] | array[low:] | array[:high] | array[:] , membuat slice dari array dimulai index low sampai index high
	var slice1 = months[4:7]
	fmt.Println(slice1)
	// Length tidak boleh lebih dari capacity
	fmt.Println(len(slice1))
	fmt.Println(cap(slice1))

	// capacity itu dari index 4 sampai desember, length itu dari index 4 sampai 7 aja yaitu 3

	// yang ini cuma ambil yang index 11
	var slice2 = months[11:]
	fmt.Println(slice2)

	// ini nambahkan array slice 2 dengan joel
	var slice3 = append(slice2, "Joel")
	fmt.Println(slice3)

	// slice 1 / joel diappend lagi dengan bukan desember
	slice3[1] = "Bukan Desember"
	fmt.Println(slice3)

	// Slice 2 ini tidak berpengaruh karena yang diatas ditampung ke variabel
	fmt.Println(slice2)

	// Ini untuk ambil semua datanya
	fmt.Println(months)

	// Ini Cara yang paling aman
	newSlice := make([]string, 2, 5)
	newSlice[0] = "Joel"
	newSlice[1] = "Effendi"

	fmt.Println(newSlice)
	fmt.Println(len(newSlice))
	fmt.Println(cap(newSlice))

	copySlice := make([]string, len(newSlice), cap(newSlice))
	// Cara copy dari new Slice ke copy Slice, length dan capacity harus sama kaya origin, bisa jga sih kalau gk semua diambil, artinya bakal kepotong arraynya
	copy(copySlice, newSlice)
	fmt.Println(copySlice)

	// Kalau array udh ditentukan lengthnya, kalau slice belum
	iniArray := [5]int{1, 2, 3, 4, 5}
	iniSlice := []int{1, 2, 3, 4, 5}

}
