package main

import "fmt"

// shortcut Filter = func(string) string
// Function type declaration
type Filter func(string) string

// Parameter kedua bentuknya function dan return valuenya string
func sayHelloWithFilter(name string, filter Filter) {
	nameFiltered := filter(name)
	fmt.Println("Hello", nameFiltered)

}

func spamFilter(name string) string {
	if name == "anjing" {
		return "*****"
	} else {
		return name
	}
}

func main() {
	// param lkedua adalah function, dimana input dan outputnya string, yang plg cocok adalah spamFilter
	sayHelloWithFilter("Joel", spamFilter)
	sayHelloWithFilter("anjing", spamFilter)
}
