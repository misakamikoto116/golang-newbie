package main

import "fmt"

func main() {
	var name string

	name = "Julian Effendi"
	fmt.Println(name)

	name = "Intana"
	fmt.Println(name)

	var age = 30
	fmt.Println(age)

	country := "Indonesia"
	fmt.Println(country)

	var (
		firstName = "Julian"
		lastName  = "Effendi"
	)

	fmt.Println(firstName)
	fmt.Println(lastName)
}
