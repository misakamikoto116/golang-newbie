package main

import (
	"fmt"
	"os"
)

// Selain dari ini silahkan liat di os package, bisa bikin folder, bkin file dan lain lain.. contoh yang paling penting adalah Getenv

// os.Args => argumen pertama adalah lokasi filenya

// Kalo dijalankan dengan go run os.go Joel Effendi
// hasilnya
// [/tmp/go-build3999519207/b001/exe/os joel effendi]
// Yang pertama lokasi, yang kedua Joel, yang ketiga Effendi
func main() {
	args := os.Args
	fmt.Println("Argument : ")
	fmt.Println(args)
	// Untuk ngambil argumen Kedua dan ketiga
	// fmt.Println(args[1])
	// fmt.Println(args[2])
	// fmt.Println(args[3])

	// Hostname => untuk melihat laptop hostnya apa, coba aja di ubuntu ketik hostname keluarnya apa
	// name, err.. karena returnnya 2 name dan error, untuk cek apakah ada error atau gk
	name, err := os.Hostname()
	if err == nil {
		fmt.Println("Hostname : ", name)
	} else {
		fmt.Println("Error", err.Error())
	}

	// Cara cobanya ketik diterminal pake :
	// export APP_USERNAME=root
	username := os.Getenv("APP_USERNAME")
	password := os.Getenv("APP_PASSWORD")

	fmt.Println(username)
	fmt.Println(password)
}
