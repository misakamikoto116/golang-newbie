package main

import (
	"flag"
	"fmt"
)

// gunanya untuk deklarasikan dan memparsing command line argument
// cara declarenya dengan :
// go run flag.go -host=localhost -user=root -password=testing
// nanti "-" tadi akan diparsing ke flag tadi
// flag.string("host", "localhost", "bla")
// pertama nama flag, kedua default value, ketiga semacam deskripsi, tapi kalo dia gk sesuai dia bakal muncul itu seperti error

// Karena flag.String itu return pointer, maka harus pake *, bisa diliat returnnya dengan cursor ke arah string
func main() {
	var host *string = flag.String("host", "localhost", "Put your Database Host")
	var user *string = flag.String("user", "root", "Put your database user")
	var password *string = flag.String("password", "root", "Put your database password")
	var number *int = flag.Int("number", 100, "Put your Number")

	// contoh lain ketika udah punya varnya
	var namaku string
	flag.StringVar(&namaku, "namaku", "Julian Effendi", "Put Your Name")

	// Memparsing data dari flag yang dideklarasi biar bisa dipake di bawah, kalo gk pake ini gk bakalan berubah kalo pake command "-", dan bakalan kena default doang
	flag.Parse()

	// Bersifat Pointer makanya pake *, kalo gk pake * maka keluarnya semacam binary array contohnya : "x0c00188070" dll
	fmt.Println("Host : ", *host)
	fmt.Println("User : ", *user)
	fmt.Println("Password : ", *password)
	fmt.Println("Number : ", *number)
	fmt.Println("Dan Namaku Adalah : ", *&namaku)
}
