package main

import "fmt"

// return biasa
func getHello(name string) string {
	if name == "" {
		return "Hello Bro"
	} else {
		return "Hello " + name
	}
}

// () kedua adalah outputnya
// kalo di java kan getfullName(): string
// Multiple Return Values
func getFullName() (string, string, string) {
	return "joel", "intan", "effendi"
}

// Named Return Values
// Kalo semuanya string bisa jga langsung gini:
// firstName, middleName, lastName string
// Gak ush pake return firstname, middlename dll
// karena udah dideclare outputnya, jdi langsung return aja automatis return yang output declare
func getFullName2() (firstName string, middleName string, lastName string) {
	firstName = "Julian"
	middleName = "Joel"
	lastName = "effendi"

	return
}

func main() {
	// return biasa
	result := getHello("Intan")
	fmt.Println(result)

	fmt.Println(getHello(""))

	// Namanya return multiple values
	// Ini sama kaya di js yang
	// [data, value] = getFullName()
	// jadi yang array Pertama jadi firstName
	firstName, _, _ := getFullName()
	fmt.Println(firstName)

	// Contoh full
	pertama, tengah, terakhir := getFullName()
	fmt.Println(pertama)
	fmt.Println(tengah)
	fmt.Println(terakhir)

	// yang ini named value
	a, b, c := getFullName2()
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
}
