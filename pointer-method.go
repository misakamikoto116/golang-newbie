package main

import "fmt"

type Man struct {
	Name string
}

// Ini Pointer, ditandakan dengan *, krena kalo gk pake itu sama dengan pass by value
func (man *Man) Married() {
	man.Name = "Mr. " + man.Name
}

func main() {
	joel := Man{"Joel"}

	// Ini duplicate habis tuh ditarok ke married, biasanya kalo pake struct method/function pake pointer

	// gk perlu pake & karena pake struct
	joel.Married()

	fmt.Println(joel.Name)
}
