package main

import "fmt"

// ini mirip kaya yang di kotlin, jadi tuh parameternya yang diujung jumlahnya dinamis, yang artinya bisa berapapun parameternya
func sumAll(numbers ...int) int {
	total := 0

	// for _, <= ignore indexnya
	for _, number := range numbers {
		total += number
	}

	return total
}

func main() {
	total := sumAll(10, 10, 20, 30, 50, 70, 70)
	fmt.Println(total)

	// ini kalo pake slice, dikasih slice...
	slice := []int{10, 20, 30, 50, 70}
	total = sumAll(slice...)
	fmt.Println(total)
}
