// String Conversion
package main

import (
	"fmt"
	"strconv"
)

func main() {
	// Ngubah String Ke boolean
	boolean, err := strconv.ParseBool("true")
	if err == nil {
		fmt.Println(boolean)
	} else {
		fmt.Println(err.Error())
	}

	// konversi ke int, 10 = basenya (Jenis angka 10 adalah desimal).. kalo binary 2, oktal 8 dll
	// Param ketiga / 64.. bitsizenya 64, artinya jumlah intnya, kan ada int32, int64 dll
	// Kalo diliat didiemin cursornya ada 2 return number dan error
	number, err := strconv.ParseInt("1000000", 10, 64)
	if err == nil {
		fmt.Println(number)
	} else {
		fmt.Println(err.Error())
	}

	// ini kebalikan, dari int ke string
	// Misal kalo 8, maka berubah jadi oktal.. namun oktalnya type string.. valuenya aja yang oktal
	value := strconv.FormatInt(1000000, 10)
	fmt.Println(value)

	// int ke string
	// valueInt, err := strconv.Itoa("2000000")

	// string ke int
	// Param kedua sengaja dibikin gini karena males pake if else, di golang kalo gk dipake harus dibikin gini.. jadi semuanya harus dipake kalo dideclare
	valueInt, _ := strconv.Atoi("2000000")
	fmt.Println(valueInt)

}
