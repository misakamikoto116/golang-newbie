package main

import "fmt"

// Tidak error karena returnnya bisa apapun.. tidak ada isi kontraknya, jadi seakan akan semuanya ngikutin interface yang kosong
func ups(i int, apapun interface{}) interface{} {
	fmt.Println(apapun)
	if i == 1 {
		return 1
	} else if i == 2 {
		return true
	} else {
		return "ups"
	}
}

func main() {
	// artinya interface ini bisa apa aja isinya
	// Contoh penggunaannya yang pake interface seperti println
	var data interface{} = ups(1, "mamba")
	fmt.Println(data)
}
