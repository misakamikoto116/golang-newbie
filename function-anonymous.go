package main

import "fmt"

type BlackList func(string) bool

func registerUser(name string, blacklist BlackList) {
	if blacklist(name) {
		fmt.Println("You Are Blocked", name)
	} else {
		fmt.Println("Welcome", name)
	}
}

//func blacklistAdmin(name string) bool {
//	return name == "admin"
//}
//
//func blacklistRoot(name string) bool {
//	return name == "root"
//}

// Anonymous function gk perlu ada nama function, sama kaya di javascript
func main() {
	// Artinya yang diblok adalah joel
	blacklist := func(name string) bool {
		return name == "joel"
	}

	registerUser("joel", blacklist)
	registerUser("effendi", blacklist)

	// cara kedua diblokir, lihat param pertama dan yang kedua yang diblok
	registerUser("joel", func(name string) bool {
		return name == "joel"
	})

}
