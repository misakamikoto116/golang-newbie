package main

import "fmt"

func main() {
	for counter := 1; counter <= 10; counter++ {
		fmt.Println("Perulangan ke ", counter)
	}

	// Ini buat array
	slice := []string{"joel", "Effendi", "intan", "hala"}

	for i := 0; i < len(slice); i++ {
		fmt.Println(slice[i])
	}

	// for range itu digunakan untuk yang collection, seperti array, map, slice
	// Ini Foreach, i = index, value = valuenya
	for i, value := range slice {
		fmt.Println("Index", i, "=", value)
	}

	// Ini Foreach tapi pake map
	// Btw ini mirip kaya assoc
	person := make(map[string]string)
	person["name"] = "Joel"
	person["title"] = "Programmer"

	for key, value := range person {
		fmt.Println(key, "=", value)
	}
}
