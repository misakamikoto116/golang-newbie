package main

import (
	"fmt"
	"strings"
)

func main() {
	// cek string di dalam string, misal julian effendi ada effendinya
	fmt.Println(strings.Contains("Julian Effendi", "Effendi"))
	fmt.Println(strings.Contains("Intana Effendi", "Julian"))

	// Hitung Jumlah huruf
	fmt.Println(len("Joel Effendi"))

	// Split Spasi jadi array.. sama kaya explode
	fmt.Println(strings.Split("Julian Effendi", " "))
	fmt.Println(strings.Split("Intana Effendi", " "))

	// huruf Kecil
	fmt.Println(strings.ToLower("Julian Effendi"))

	// Ke Kapital
	fmt.Println(strings.ToUpper("julian effendi"))

	// Ke capitalize
	fmt.Println(strings.ToTitle("joel effendi"))

	// Hilangkan spasi, mirip kaya str_replace tapi replacenya cuma bisa "" artinya hapus yang dimaksd
	fmt.Println(strings.Trim("      Eko Kurniawan     ", " "))

	// String replace
	// Ubah kata Eko ke budi
	fmt.Println(strings.ReplaceAll("Eko Joko Eko", "Eko", "Budi"))
}
