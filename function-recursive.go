package main

import "fmt"

// ini loop biasa
// Misal value = 1 maka :
// Faktorial 5*4*3*2*1
func factorialLoop(value int) int {
	result := 1
	for i := value; i > 0; i-- {
		result *= i // result = result * i
	}

	return result
}

// Yang ini cara recursive dan lebih gampang
// Sama kaya java dimana dia panggil diri sendiri terus, dan berhenti ketika 1 (di if tidak panggil factorialRecursive lagi)
// Nah lalu jika diatas 1 maka bakal dikurangin 1 sambil panggil lagi
func factorialRecursive(value int) int {
	if value == 1 {
		return 1
	} else {
		return value * factorialRecursive(value-1)
	}
}

func main() {
	loop := factorialLoop(10)
	fmt.Println(loop)
	// fmt.Println(5 * 4 * 3 * 2 * 1)

	recursive := factorialRecursive(10)
	fmt.Println(recursive)

}
