// Pass by value
// address1 := Address{"Samarinda", "Kalimantan Timur", "Indonesia"}
// address2 := address1
// address2.City = "Jakarta"
// Outputnya sih address2 ini bakal copy address1, hanya saja city diganti ke Jakarta

// Pass by reference / Pointer
// address2 := &address1
// jadi nanti address 2 dan 1 sama, address2 berubah address1 bakal berubah jga sama, cman bedanya output &{Jakarta dll} <= ada &nya

package main

import "fmt"

type Address struct {
	City, Province, Country string
}

// Bikin pointer di function dengan menggunakan *Address.. ini lbh harus hati hati karena mengubah data aslinya, hanya saja ini copy data.. mirip kaya setState direact / change state.. jdi jangan ubah langsung

// Kalo dia gk pake *Address artinya dia bakal copy data cuman data origin gk berubah
func changeCountryToIndonesia(address *Address) {
	address.Country = "Indonesia"
}

// ini dikasih *Address karena address2 adalah pointer dari address1
// var address2 *Address = &address1

// Address2 = &Address harus pake "&" karena dia pointer
func main() {
	var address1 Address = Address{"Samarinda", "Kalimantan Timur", "Indonesia"}
	var address2 *Address = &address1
	var address3 *Address = &address1
	// var address4 *Address = &Address{"Subang", "Jawa Barat", "Indonesia"}

	address2.City = "Bandung"

	// address2 pindah pointer dan gk ngubah address1, bkin address baru
	// address2 = &Address{"Jakarta", "DKI Jakarta", "Indonesia"}

	// yang ini bakal memaksa addressnya kesini, artinya kalo pake * maka semua yang mengacu ke struct memory akan berubah ke ini
	*address2 = Address{"Jakarta", "DKI Jakarta", "Indonesia"}

	fmt.Println(address1)
	fmt.Println(address2)
	fmt.Println(address3)

	// Bkin Pointer baru dari variabel yang sudah ada tapi kosong isinya
	var address4 *Address = new(Address)
	address4.City = "Jakarta"
	fmt.Println(address4)

	var alamat = Address{
		City:     "Indramayu",
		Province: "Jawa Barat",
		Country:  "Taiwan",
	}

	// Isinya *Address dan &alamat.. artinya dia pointer dan akan mengubah alamat yang pertama jga, jdi semacam shortcut gitu.. ngubah B, yang A keubah jga
	var alamatPointer *Address = &alamat
	changeCountryToIndonesia(alamatPointer)
	fmt.Println(alamat)
}
