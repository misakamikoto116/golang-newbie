package main

import "fmt"

func main() {
	const (
		firstName string = "Julian"
		lastName         = "effendi"
		value            = 1000
	)

	fmt.Println(firstName)
	fmt.Println(lastName)
	fmt.Println(value)
}
