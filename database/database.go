package database

var connection string

// Ini Private, gak boleh dimodif secara langsung
func init() {
	connection = "MySQL"
}

// connection boleh dipanggil disini karena ini bentuknya public
func GetDatabase() string {
	return connection
}
