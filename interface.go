package main

import "fmt"

// Interface seperti abstract class, mirip kaya interface di Php, tapi di golang semuanya otomatis
type HasName interface {
	GetName() string
}

func sayHello(hasName HasName) {
	fmt.Println("Hello", hasName.GetName())
}

type Person struct {
	Name string
}

// Harus pakai gini agar contract interfacenya.. karena yang berhak pakai sayHello adalah yang punya GetName saja [sayHello(hasName HasName)]
// Dan Returnnya pun harus string jga karena di HasName itu GetNamenya return string
func (person Person) GetName() string {
	return person.Name
}

type Animal struct {
	Name string
}

func (animal Animal) GetName() string {
	return animal.Name
}

func main() {
	var people Person
	people.Name = "intana"
	sayHello(people)

	// Bisa dijalankan karena function Animal punya function GetName, maksdnya ngikutin rules getName / return string
	cat := Animal{
		Name: "Push!",
	}
	sayHello(cat)
}
