package main

import "fmt"

// Hanya dari 1 sampai 5, sisanya dibreak
func main() {
	for i := 0; i < 10; i++ {
		if i == 5 {
			break
		}

		fmt.Println("Perulangan Ke - ", i)
	}
}
